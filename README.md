# boutpy - tutorial

by J.G. Chen, BOUT++ team, email: cjgls@pku.edu.cn

**BOUTPY** is a ``Python2.x`` package intended to provide some common tools
required for performing Plasmas Fluid Simulations with
[BOUT++](http://boutproject.github.io/index.html) framework based on
`bout-master/tools/pylib` at 06/18/2016. The BOUT++ boutpy in the latest release
[BOUT++ v4.0.0](https://github.com/boutproject/BOUT-dev/releases/tag/v4.0.0) is
``Python3.x`` supported.

**Download** **BOUTPY** is accessible on
[GitLab](https://gitlab.com/conderls/boutpy)

**Bugs Report** The most bugs and issues are managed using the
[issue tracker](https://gitlab.com/conderls/boutpy/issues).
All suggestions, comments and feature requests are gladly wellcome and
appreciated.

The document for the boutpy package is available at
[ReadTheDocs](http://boutpy.readthedocs.io). The user manual for the latest
BOUT++ are available [here](http://bout-dev.readthedocs.io/en/latest/).
